import React, { useMemo, useEffect, useState } from 'react';
import { DataSourceSettings, PanelProps } from '@grafana/data';
import { AlertStatusOptions, PromRulesResponse } from 'types';
import { BackendSrv, getBackendSrv } from '@grafana/runtime';

interface Props extends PanelProps<AlertStatusOptions> {}

export const AlertStatusPanel: React.FC<Props> = ({ options, data, width, height }) => {
  const backend = useMemo(getBackendSrv, []);

  // get data sources
  const [datasources, setDatasources] = useState<DataSourceSettings[]>([]);
  useEffect(() => {
    let active = true;
    load();
    return () => {
      active = false;
    };

    async function load() {
      const res = await getAllDataSources(backend);
      if (!active) {
        return;
      }
      setDatasources(res);
    }
  }, [backend]);

  // get alerts
  const [alerts, setAlerts] = useState<PromRulesResponse[]>([]);
  //TODO
  // url: `/api/prometheus/${getDatasourceAPIId(dataSourceName)}/api/v1/rules`
  useMemo(() => setAlerts([]), []);
  
  // render output
  return (
    <div>
      <div>
        <p> Data Sources </p>
        {datasources.map(ds => (
          <p>
            {ds.id}: {ds.name}, {ds.type}
          </p>
        ))}
      </div>
      <div>
        <p> Matched Alerts </p>
        {alerts.map(a => (
          <p>
            {Object.values(a).map(m => (
              <p> {m} </p>
            ))}
          </p>
        ))}
      </div>
    </div>
  );
};

async function getAllDataSources(backend: BackendSrv): Promise<DataSourceSettings[]> {
  return backend.get(`/api/datasources`);
}
