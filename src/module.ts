import { PanelPlugin } from '@grafana/data';
import { AlertStatusOptions } from './types';
import { AlertStatusPanel } from './AlertStatusPanel';

export const plugin = new PanelPlugin<AlertStatusOptions>(AlertStatusPanel).setPanelOptions(builder => {
  // Add Base Options
  builder.addNumberInput({
    name: 'Max items',
    path: 'maxItems',
    defaultValue: 20,
    category: ['Options'],
  });

  // Add alert(s) to monitor
  builder
    .addTextInput({
      path: 'alertFilter',
      name: 'Alerts to monitor',
      defaultValue: '.*',
      category: ['Monitored Alerts'],
    })
    .addTextInput({
      path: 'labelFilter',
      name: 'Labels that must match (regex)',
      defaultValue: '.*',
      category: ['Monitored Alerts'],
    })
    .addBooleanSwitch({
      path: 'treatInactiveAsFiring',
      name: 'Treat inactive alerts as firing',
      defaultValue: false,
      category: ['Monitored Alerts'],
    });

  return builder;
});
